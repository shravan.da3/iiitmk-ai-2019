# ===================
# Mid Sem Exam
# ===================
# - Please copy the file called `midsem.py` into your **OWN** folder.
# - From now on, everything must be done in your own copy of the file.
# - Edit the file to solve the problem stated in it.
# - Save it, then commit your code and push to your fork of the project.
# - Make sure the tests are passing. If tests fail, you can look at the tests and figure out what you did wrong.
# - Submit a merge request to the Master branch of the class project.
# - Make sure that rebase issues do not exist or give rebase permissions to reviewer while creating the merge request
# - Once you are sure you are done and want to submit, simply mention @theSage21 in the comments of the merge request.
# - The timestamp on this comment shows when you finished your exam. You must finish within the exam time limit.

# NOTE: If at any time you have questions, please open an issue in the class project

# ===================
# Statement
# ===================

# You must write a function that returns a location in a game of tic-tac-toe
# your agent must try to win as many games as possible
# The board is a tuple of 3 strings. For example the starting position is:
#     ('   ',
#      '   ',
#      '   ')
# If your agent decides to put your symbol in coordinates (0, 0) the board would look like:

#     ('x  ',
#      '   ',
#      '   ')

# Your agent must return two coordinates whenever it is called
# for example in order to make the move shown above your function
# must be something like this:

# def agent(board, your_symbol):
#     return 0, 0


# ===================
# Scoring
# ===================

# The scoring is simple.
# For every game you win, you get 1 point
# For everything else you get 0 point (lose a game/end game due to invalid move)
# There are two test cases for this problem
# Each test case has a single opponent algorithm your agent must try to win against.
# You can see the jobs in the merge request to see how your submission is doing
import random


def neighbours(n):
    n_e = []
    for i in n:
        for x in range(-1, 2):
            for y in range(-1, 2):
                if x == 0 and y == 0:
                    pass
                else:
                    n_e.append([i[0] + x, i[1] + y])
    return n_e


def agent(board, your_symbol):
    ...  # TODO: Please complete the function
    moves = [[0, 0], [0, 1], [0, 2], [1, 0], [1, 1], [1, 2], [2, 0], [2, 1], [2, 2]]
    mine = []
    pc = []
    empty = []
    if your_symbol == "x":
        pc_sym = "o"
    else:
        pc_sym = "x"
    for i in moves:
        if board[i[0]][i[1]] == your_symbol:
            mine.append(i)
        elif board[i[0]][i[1]] == pc_sym:
            pc.append(i)
        else:
            empty.append(i)
    n_m = neighbours(mine)
    n_p = neighbours(pc)

    k = None
    if not mine:
        k = random.choice(empty)
    else:
        for i in n_m:
            if i in mine:
                l = neighbours([i])
                for j in l:
                    if j in empty:
                        k = j
                        break
            break
        if k == None:
            for i in n_p:
                if i in mine:
                    l = neighbours([i])
                    for j in l:
                        if j in empty:
                            k = j
                            break
                break
    if k == None:
        for i in n_p:
            if i in empty:
                k = i

    return (k[0], k[1])
