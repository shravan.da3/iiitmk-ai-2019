# ===================
# Mock Mid Sem
# ===================
# - Please copy the file called `mock_midsem.py` into your **OWN** folder.
# - From now on, everything must be done in your own copy of the file.
# - Edit the file to solve the problem stated in it
# - commit your code and push to your fork of the project
# - submit a merge request to the Master branch of the class project.
# - make sure that rebase issues do not exist
# - make sure the tests are passing. If tests fail, you can look at the tests and figure out what you did wrong
# - Once you are sure you are done and want to submit, simply mention @theSage21 in the comments of the merge request.
# - The timestamp on this comment shows when you finished your exam. You must finish within the exam time limit.


# ===================
# Statement
# ===================
# For the mock problem, you need to implement a function which is capable of
# addition, multiplication, division and subtraction. Given string
# representations of what operation to do and the string representations of the
# numbers you must operate on, your function must return the string
# representation of the result


def submission(operation: str, a: str, b: str) -> str:
    if operation == "+":
        return str(float(a) + float(b))
    # TODO: fill the rest of the function
